import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

// *NGX-SOCKET
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
const config: SocketIoConfig = { url: 'http://localhost:5000', options: {} };

// * ChartsModule
import { ChartsModule } from 'ng2-charts';

import { AppComponent } from './app.component';

// * COMPONENTS
import { GraficaComponent } from './components/grafica/grafica.component';


@NgModule({
  declarations: [
    AppComponent,
    GraficaComponent,
  ],
  imports: [
    BrowserModule,
    SocketIoModule.forRoot(config),
    ChartsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
